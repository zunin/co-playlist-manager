
import React from 'react';
import { AxiosInstance } from 'axios';
import { Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import swatch from './swatch';

const playlistId = "7tbcDC8mxhDRiqB1BM91Pd"



interface SpotifyPlaylistProps {
    client: AxiosInstance
}

interface SpotifyPlaylistState {
    playlist?: SpotifyPaging<SpotifyPlaylistTrack>
}

class SpotifyPlaylist extends React.Component<SpotifyPlaylistProps, SpotifyPlaylistState> {
    constructor(props: SpotifyPlaylistProps) {
        super(props);
        this.state = {
            playlist: undefined
        }
        const { client } = this.props;
        client.get(`v1/playlists/${playlistId}/tracks`).then((resp) => {
            if (resp.status === 200) {
                return resp.data as SpotifyPaging<SpotifyPlaylistTrack>
            }
            debugger;
            return null;
        }).then((data) => {
            if (data) {
                this.setState({playlist: data});
            }
        });
    }
    render() {
        const { playlist } = this.state;
        if (playlist) {
            return <Table>
                <TableHead>
                    <TableRow>
                        <TableCell style={{color: swatch.white}}>#</TableCell>
                        <TableCell style={{color: swatch.white}}>Title</TableCell>
                        <TableCell style={{color: swatch.white}}>Artist</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                {playlist.items.map(((track, index) => {
                    return <TableRow key={index.toString()}>
                    <TableCell style={{color: swatch.white}}>{index + 1}</TableCell>
                    <TableCell style={{color: swatch.white}}>{track.track.name}</TableCell>
                    <TableCell style={{color: swatch.white}}>{track.track.artists.map(x => x.name).join(", ")}</TableCell>
                    </TableRow >
                }))}
                </TableBody>
            </Table>
        }
        return <div>
            No playlist found.
        </div>
    }
}

export default SpotifyPlaylist