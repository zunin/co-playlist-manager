import React from 'react';
import { AxiosInstance } from 'axios';
import { Avatar } from '@material-ui/core';


interface SpotifyProfileProps {
    client: AxiosInstance
}



interface SpotifyProfileState {
    userData?: SpotifyPrivateUser | null;
}

class SpotifyProfile extends React.Component<SpotifyProfileProps, SpotifyProfileState> {
    constructor(props: SpotifyProfileProps) {
        super(props);
        this.state = {
            userData: undefined
        }
        const { client } = this.props;
        client.get("v1/me").then((resp) => {
            if (resp.status === 200) {
                return resp.data as SpotifyPrivateUser
            }
            return null;
        }).then((profile) => {
            if (profile) {
                this.setState({userData: profile});
            }
        });
    }
    render() {
        const { userData } = this.state;
        if (userData) {
            return <Avatar alt={userData.display_name} src={userData.images[0].url} /> ;
        }

        return <div>
            No data
        </div>
    }
}

export default SpotifyProfile