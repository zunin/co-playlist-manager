interface SpotifyExternalURLs {
    [key: string]: string
}

interface SpotifyFollowers {
    href: string | null;
    total: number;
}

interface SpotifyImage {
    height: number | null;
    width: number | null;
    url: string
}

interface SpotifyPrivateUser {
    country: string;
    display_name: string;
    explicit_content: {
        filter_enabled: boolean;
        filter_locked: boolean;
    }
    external_urls: Array<SpotifyExternalURLs>;
    followers: SpotifyFollowers
    href: string,
    id: string,
    images: Array<SpotifyImage>;
    product: string;
    type: string;
    uri: string
}

interface SpotifyUser {
    display_name?: string | null;
    external_urls: SpotifyExternalURLs;
}

interface SpotifyAlbum {
    
}

interface SpotifySimplifiedArtist {
    external_urls: SpotifyExternalURLs;
    href: string;
    id: string;
    name: string;
    type: string;
    uri: string;
}

interface SpotifyExternalIDs {
    [identifierType: string]: string
}

interface SpotifyTrackLink {
    external_urls: SpotifyExternalURLs;
    href: string;
    id: string;
    type: string;
    uri: string;
}

interface SpotifyTrack {
    album: SpotifyAlbum;
    artists: Array<SpotifySimplifiedArtist>;
    available_markets: Array<string>;
    disc_number: number;
    duration_ms: number;
    explicit: boolean;
    external_ids: SpotifyExternalIDs;
    href: string;
    id: string;
    is_playable: boolean;
    linked_from: SpotifyTrackLink
    restrictions: SpotifyRestrictions;
    name: string;
    popularity: number;
    preview_url: string;
    track_number: string;
    type: string;
    uri: string;
}

interface SpotifyRestrictions {

}

interface SpotifyPlaylistTrack {
    added_at: Date;
    added_by?: SpotifyUser | null;
    is_local: boolean;
    track: SpotifyTrack;
}

interface SpotifyPaging<T> {
    href: string;
    items: Array<T>;
    limit: number;
    next: string;
    offset: number;
    previous: string;
    total: number;
}