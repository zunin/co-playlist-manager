import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Button } from '@material-ui/core';
import SpotifyProfile from './SpotifyProfile';
import axios from 'axios';
import SpotifyPlaylist from './SpotifyPlaylist';

const spotify = {
  ClientID: "fb56ecc024e84e60ba02eac0cd6f78ef",
};

const scope = [
  "playlist-read-collaborative",
  "streaming",
  "user-read-private" //profile picture
].join(" ");


const redirectURI = "https://co-playlist-manager.herokuapp.com"

interface AppProps {

}

export interface AppState {
  userToken: string | null
}

class App extends React.Component<AppProps, AppState> {
  constructor(props: AppProps) {
    super(props);    
    let userToken = null;
    try {
      const hash = window.location.hash.substr(1);
      userToken = hash.split("=")[1].split("&")[0];
    } catch (_) { 
      userToken = null
    }

    this.state = {
      userToken: userToken
    };
  }
  
  authWithSpotify() {
    var url = "https://accounts.spotify.com/authorize?response_type=token" +
      `&client_id=${spotify.ClientID}` + 
      `&scope=${encodeURIComponent(scope)}` + 
      `&redirect_uri=${encodeURIComponent(redirectURI)}`;

    window.location.href = url;
  }

  render() {
    const { userToken } = this.state;

    const wrapper = (element?: JSX.Element) => {
      return (
      <div className="App">
        <header className="App-header">
         {element}
        </header>
      </div>
    )};

    if (!userToken) {
      return wrapper(<div>
        <img src={logo} className="App-logo" alt="logo" />
        <h1>First, log in to spotify</h1>
        <Button onClick={this.authWithSpotify.bind(this)} color="primary" variant="contained">Log in</Button>
      </div>);
    }
    const spotifyInstance = axios.create({
       baseURL: "https://api.spotify.com/",
       headers: {'Authorization': `Bearer ${userToken}`},
       timeout: 1000,
    });
    return wrapper(<div>
      <SpotifyProfile client={spotifyInstance} />
      <SpotifyPlaylist client={spotifyInstance} />
    </div>)
  }
  
}

export default App;
