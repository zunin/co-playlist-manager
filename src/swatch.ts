export default {
    black: "#4F484B",
    paleTeal: "#68A9B2",
    paleYellow: "#FFF2CF",
    orange: "#FFAD6F",
    red: "#FF3F63",
    white: "#ffffff"
};
